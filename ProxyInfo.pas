unit ProxyInfo;

interface

uses Registry, WinApi.Windows, System.SysUtils, System.TypInfo;

type
  TProxyInfo = class
  private
    class function ExtractProxy: string;
    class function GetRegistryData<T>(AName: string): variant;
  public
    class function Host(ACheckActive: boolean = false): string;
    class function Port(ACheckActive: boolean = false): integer;
    class function Active: boolean;
  end;

implementation

{ TProxyInfo }

class function TProxyInfo.Active: boolean;
begin
  Result := Self.GetRegistryData<integer>('ProxyEnable') <> 0;
end;

class function TProxyInfo.ExtractProxy: string;
var
  reg: TRegistry;
  proxy1: string;
  x: TArray<string>;
  active1: boolean;
begin
  proxy1 := Self.GetRegistryData<string>('ProxyServer');
  Result := '';
  if proxy1 <> '' then begin
    x := proxy1.Split([';', '=', ':']);
    if length(x) >= 3 then begin
      Result := x[1] + ':' + x[2];
    end else if (length(x) = 2) then begin
      Result := x[0] + ':' + x[1];
    end;
  end;
end;

class function TProxyInfo.GetRegistryData<T>(AName: string): variant;
var
  Info: PTypeInfo;
  reg: TRegistry;
  Data: PTypeData;
begin
  reg := TRegistry.Create;
  Info := System.TypeInfo(T);
  if Info <> nil then begin
    try
      reg.RootKey := HKEY_CURRENT_USER;
      reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Internet Settings', false);
      if Info^.Kind = tkInteger then
        Result := reg.ReadInteger(AName);
      if Info^.Kind = tkUString then
        Result := reg.ReadString(AName);
      if Info^.Kind = tkEnumeration then
        Result := reg.ReadInteger(AName);
    except

    end;
  end;
  reg.Free;
end;

class function TProxyInfo.Host(ACheckActive: boolean): string;
var
  proxy1: string;
begin
  proxy1 := Self.ExtractProxy;
  Result := '';
  if (proxy1 <> '') then
    Result := proxy1.Split([':'])[0];
  if (ACheckActive) and (not Self.Active) then
    Result := '';
end;

class function TProxyInfo.Port(ACheckActive: boolean): integer;
var
  proxy1: string;
begin
  proxy1 := Self.ExtractProxy;
  Result := 0;
  if (proxy1 <> '') then
    Result := proxy1.Split([':'])[1].ToInteger;
  if (ACheckActive) and (not Self.Active) then
    Result := 0;
end;

end.
