unit DownloadThread;

interface

uses System.classes, IdHTTP, IdMultiPartFormData;

type
  TOnFinish = procedure(Sender: TObject; AMemoryStream: TMemoryStream; AData: pointer) of Object;

type
  TDownloadThread = class(TThread)
  protected

  private
    MemoryStream1: TMemoryStream;
    URL: string;
    B: integer;
    FOnFinish: TOnFinish;
    procedure Progress;
    procedure Execute; override;
    procedure OnFinishSync;
    procedure SetOnFinish(const Value: TOnFinish);
  public
    IdHTTP: TIdHTTP;
    Data: pointer;
    PostData: TIdMultiPartFormDataStream;
    constructor Create(AURL: string; APost: TIdMultiPartFormDataStream = nil);
    destructor Destroy; override;
  published
    property OnFinish: TOnFinish read FOnFinish write SetOnFinish;
  end;

implementation

{ TUpdateThread }

procedure TDownloadThread.Progress;
begin

end;

procedure TDownloadThread.SetOnFinish(const Value: TOnFinish);
begin
  FOnFinish := Value;
end;

procedure TDownloadThread.Execute;
var
  content: string;
begin
  try
    if assigned(PostData) then
      IdHTTP.Post(URL, PostData, MemoryStream1)
    else
      IdHTTP.Get(URL, MemoryStream1)
  except

  end;
  MemoryStream1.Position := 0;
  Synchronize(OnFinishSync);
end;

procedure TDownloadThread.OnFinishSync;
begin
  if (assigned(FOnFinish)) then
    FOnFinish(self, MemoryStream1, Data);
end;

constructor TDownloadThread.Create(AURL: string; APost: TIdMultiPartFormDataStream = nil);
begin
  MemoryStream1 := TMemoryStream.Create;
  PostData := APost;
  IdHTTP := TIdHTTP.Create(nil);
  URL := AURL;
  FreeOnTerminate := True;
  inherited Create(True);
end;

destructor TDownloadThread.Destroy;
begin
  IdHTTP.Free;
  if (assigned(PostData)) then
    PostData.Free;
  inherited Destroy;
end;

end.
