unit CUtils.Common;

interface

uses System.Classes, System.SysUtils, Winapi.Windows, Vcl.Forms;

function GetExeVersionString(AFile: string = ''; AFormat: string = '%d.%d.%d.%d'): string;

implementation

function GetExeVersionString(AFile: string = ''; AFormat: string = '%d.%d.%d.%d'): string;
var
  InfoSize, FileSize: cardinal;
  Info: Pointer;
  FileInfo: PVSFixedFileInfo;
  Major1, Minor1, Release1, Build1: Integer;
begin
  try
    if (AFile = '') then
      InfoSize := GetFileVersionInfoSize(pChar(Application.ExeName), FileSize)
    else
      InfoSize := GetFileVersionInfoSize(pChar(AFile), FileSize);

    if InfoSize = 0 then
      raise Exception.Create('Error retrieving Info size.');
    GetMem(Info, InfoSize);
    try
      GetFileVersionInfo(pChar(Application.ExeName), 0, InfoSize, Info);
      VerQueryValue(Info, '\', Pointer(FileInfo), FileSize);
      Major1 := FileInfo.dwFileVersionMS shr 16;
      Minor1 := FileInfo.dwFileVersionMS and $FFFF;
      Release1 := FileInfo.dwFileVersionLS shr 16;
      Build1 := FileInfo.dwFileVersionLS and $FFFF;
    finally
      FreeMem(Info);
    end;
    Result := Format(AFormat, [Major1, Minor1, Release1, Build1]);
  except
    Result := '';
  end;
end;

end.
